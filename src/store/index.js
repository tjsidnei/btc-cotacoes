import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    name: 'Sidnei'
  },
  mutations: {
    change (state, payload) {
      state.name = payload.newName
    }
  },
  actions: {
    change ({ commit }, payload) {
      commit('change', payload)
    }
  },
  modules: {
  }
})
